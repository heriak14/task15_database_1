# 1
SELECT * FROM pc WHERE model RLIKE('1{2,}');
# 2
SELECT * FROM outcome WHERE month(`date`) = 3;
# 3
SELECT * FROM outcome_o WHERE day(`date`) = 14;
# 4
SELECT `name` FROM ships WHERE `name` LIKE('W%n');
# 5
SELECT `name` FROM ships WHERE `name` RLIKE('e{2}');
# 6
SELECT `name`, launched FROM ships WHERE `name` NOT LIKE('%a');
# 7
SELECT `name` FROM battles WHERE `name` LIKE('% %') AND `name` NOT LIKE('%c');
# 8
SELECT * FROM trip WHERE  time(time_out) BETWEEN '12:00:00' AND '17:00:00';
# 9
SELECT * FROM trip WHERE  time(time_in) BETWEEN '17:00:00' AND '23:00:00';
# 10
SELECT `date` FROM pass_in_trip WHERE place LIKE('1%');
# 11
SELECT `date` FROM pass_in_trip WHERE place LIKE('%c');
# 12
SELECT `name` FROM passenger WHERE `name` LIKE '% C%';
# 13
SELECT `name` FROM passenger WHERE `name` RLIKE '.+ [^J].+';
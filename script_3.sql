# 1
SELECT maker, type, speed, hd FROM pc JOIN product ON pc.model = product.model WHERE hd <= 8;
# 2
SELECT maker FROM pc JOIN product ON pc.model = product.model WHERE speed >= 600;
# 3
SELECT maker FROM laptop JOIN product ON laptop.model = product.model WHERE speed <= 500;
# 4
SELECT l1.model model1, l2.model model2, l1.hd, l1.ram FROM laptop l1 JOIN laptop l2 ON l1.hd = l2.hd AND l1.ram = l2.ram WHERE l1.model > l2.model;
# 5
SELECT c1.country, c1.type type1, c2.type type2 FROM classes c1 JOIN classes c2 ON c1.country = c2.country WHERE c1.type != c2.type; 
# 6
SELECT pc.model, maker FROM pc JOIN product ON pc.model = product.model WHERE price < 600;
# 7
SELECT printer.model, maker FROM printer JOIN product ON printer.model = product.model WHERE price < 600;
# 8
SELECT maker, pc.model, price FROM pc LEFT JOIN product ON pc.model = product.model;
# 9
SELECT maker, pc.model, price FROM pc LEFT JOIN product ON pc.model = product.model WHERE price IS NOT NULL;
# 10
SELECT maker, type, l.model, speed FROM laptop l JOIN product ON l.model = product.model WHERE l.speed > 600;
# 11
SELECT name, displacement FROM ships s LEFT JOIN classes c ON s.class = c.class; 
# 12
SELECT battle, `date` FROM outcomes o JOIN battles b ON o.battle = b.name WHERE o.result = 'OK';  
# 13
SELECT name, country FROM ships JOIN classes ON ships.class = classes.class;
# 14
SELECT DISTINCT plane, name FROM trip JOIN company ON trip.ID_comp = company.ID_comp WHERE plane = 'Boeing';
# 15
SELECT name, date FROM passenger LEFT JOIN pass_in_trip ON passenger.ID_psg = pass_in_trip.ID_psg;